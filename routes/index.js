const express = require('express');
const db = require('../src/service.configs/MYSQL.config');
const multer = require('multer');
const path = require('path');
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const md5 = require('js-md5');
const router = express.Router();
let curren_token = null;
const fileStoragePath = 'https://s3.eu-west-2.amazonaws.com/englishteachblog';
const fileUploadPath = fileStoragePath;

AWS.config.accessKeyId = process.env.TAS_KEY_ID;
AWS.config.secretAccessKey = process.env.TAS_SECRET;

const storage = multerS3({
	s3:new AWS.S3(),
	bucket:'englishteachblog',
	acl:'public-read',
	contentType: (req, file, cb) => {
		cb(null, file.mimetype);
	},
	metadata: function (req, file, cb) {
		cb(null, {'Content-Type': file.mimetype});
	},
	key: function (req, file, cb) {
		let len = file.originalname.toString().split('.').length;
		let ext = file.originalname.toString().split('.')[len - 1];
		cb(null, `${Date.now()}${parseInt(Math.random()*1000)}.${ext}`);
	}
});
const formDataParser = multer({ storage: storage });

let content = {
	ru:{
		menu:require('../src/service.configs/content.ru.config.json'),
		content:{
			main:require('../src/service.configs/content.jsons/main.json'),
			about:require('../src/service.configs/content.jsons/about.json')
		}
	}
};

function auth(req, res, next) {
	if (!('token' in req.cookies))
		return res.send({err:true, messgae: 'Invalid token!'});
	if (req.cookies.token !== curren_token)
		return res.send({err:true, messgae: 'Invalid token!'});
	next();
}

router.all('/admin', auth);

/* GET home page. */
router.get('/', (req, res) => {
	res.render('index', {
		menu:content.ru.menu,
		docContent:content.ru.content.main,
		activeCode:'mainMenuItem'
	});
});

router.get('/about', (req, res) => {
	res.render('about', {
		menu:content.ru.menu,
		docContent:content.ru.content.about,
		activeCode:'about'
	});
});

router.get('/blog', (req, res) => {
	res.render('blog', {
		menu:content.ru.menu,
		docContent:content.ru.content.blog,
		activeCode:'blog'
	});
});

router.get('/topics', async (req, res) => {
	try {
		let getTopicsSQL = db.SQL.SELECT.TOPICS({});
		let topics = await db.runSQL(getTopicsSQL);
		if (topics.err)
			throw new Error('SQL error! Get topics!');
		topics = topics.result;
		for (let i in topics){
			let getSubtopicsSQL = db.SQL.SELECT.SUBTOPICS({topic_id:topics[i].id});
			let subtopics = await db.runSQL(getSubtopicsSQL);
			if (subtopics.err)
				throw new Error('SQL error! Getting subtopics');
			topics[i].subtopics = subtopics.result;	
		}	
		res.render('topics', {
			menu:content.ru.menu,
			docContent:content.ru.content.topics,
			activeCode:'topics',
			topics:topics
		});
	} catch(err) {
		res.render('topics', {
			menu:content.ru.menu,
			docContent:content.ru.content.topics,
			activeCode:'topics',
			topics:null
		});
	}
});

router.get('/login', async (req, res) => {
	res.render('login', {
		menu:content.ru.menu
	});
});

router.post('/login', async (req, res) => {
	try {
		if (req.body.login === 'root' && md5(req.body.password) === process.env.TAS_PASS.toString()) {
			curren_token = md5(`${md5(req.body.password)}${new Date()}`);
			return res.send({
				err: null,
				data: {
					token: curren_token
				}
			});
		} else throw new Error('');
	} catch (err){
		res.send({
			err: 'Invalid login or password!',
			data: null
		});
	}
});

router.get('/admin', async (req, res) => {
	let sql = db.SQL.SELECT.TOPICS({});
	let topics = await db.runSQL(sql);
	let buf_t = {};
	if (topics.result === undefined)
		return res.send('Error 500!');
	if (topics.err == null) {
		topics = topics.result;
		buf_t.length = topics.length;
		for (let i = 0; i < topics.length; i ++){
			let sql = db.SQL.SELECT.SUBTOPICS({topic_id:topics[i].id});
			buf_t[topics[i].id] = topics[i];
			let sub_t_result = (await db.runSQL(sql)).result;
			buf_t[topics[i].id].subtopics = {};
			buf_t[topics[i].id].subtopics.length = sub_t_result.length;
			for (let j = 0; j < sub_t_result.length; j ++) {
				buf_t[topics[i].id].subtopics[sub_t_result[j].id] = sub_t_result[j];
			}
		}
	}
	let getPostsSQL = db.SQL.SELECT.ALLPOSTS;
	let posts = await db.runSQL(getPostsSQL);
	res.render('admin', {
		menu:content.ru.menu,
		topics: buf_t,
		posts: posts
	});
});
router.post('/api/addPost', auth);
router.post('/api/addPost', formDataParser.any(), async (req, res) => {
	try {
		let data = req.body;
		data.files = req.files;
		data.description = data.description.replace(/'/g, '\\\'');
		//TODO: Validate body
		let addPostSQL = db.SQL.INSERT.POST({text:data.description, sub_topic:data.sub_topic, subject:data.subject});
		let newPostResult = await db.runSQL(addPostSQL);
		if (newPostResult.err)
			throw new Error('SQL error!');
		let postInsertId = newPostResult.result.insertId;
		let addFileSQL = '';
		let addPostFileLinkSQL = '';
		let resultFile = null;
		let resultLink = null;
		let resData = {
			err:false,
			post_id:postInsertId,
			files:[]
		};

		if (data.files.length === 0)
			return res.send(resData);

		for (let i in data.files) {
			addFileSQL = db.SQL.INSERT.FILE({
				name:data.files[i].key, 
				storage:fileStoragePath,
				original_name:data.files[i].originalname
			});
			resultFile = await db.runSQL(addFileSQL);
			if (resultFile.err)
				throw new Error('SQL error!');
			addPostFileLinkSQL = db.SQL.INSERT.POST_FILE({
				post:postInsertId, 
				file:resultFile.result.insertId
			});
			resultLink = await db.runSQL(addPostFileLinkSQL);
			if (resultLink.err)
				throw new Error('SQL error on linking post - file!');
			resData.files.push({
				filename:data.files[i].filename,
				storage:fileStoragePath,
				fileId:resultFile.result.insertId,
				postId:postInsertId
			});
		}
		return res.send(resData);
	} catch(err){
		return res.send({err:true, message:err.message});
	}
});
router.post('/api/addTopic', auth);
router.post('/api/addTopic', async (req, res) => {
	try {
		if (!('topic' in req.body))
			throw new Error('Error in input data!');
		let sql = db.SQL.SELECT.TOPICS({topic:req.body.topic});
		let res_data = await db.runSQL(sql);
		if (res_data.err)
			throw new Error('SQL error!');
		if (res_data.result.length != 0)
			throw new Error('Duplicated topic!');	
		sql = db.SQL.INSERT.TOPICS({topic:req.body.topic});
		res_data = await db.runSQL(sql);
		let body = {
			err:false,
			message:'ok',
			topic: {
				id: res_data.result.insertId,
				name: req.body.topic
			}
		};
		return res.send(body);
	} catch (err) {
		return res.send({err:true, message:err.message});
	}
});
router.post('/api/addSubTopic', auth);
router.post('/api/addSubTopic', async (req, res) => {
	try {
		if (!('topic_id' in req.body) && !('subtopic' in req.body))
			throw new Error('Error in input data!');
		let sql = db.SQL.SELECT.SUBTOPICS({topic_id:req.body.topic_id, sub_topic:req.body.subtopic});
		let res_data = await db.runSQL(sql);
		if (res_data.err)
			throw new Error('SQL Error!');
		if (res_data.result.length != 0)
			throw new Error('Dublicated SubTitle!');	
		sql = db.SQL.INSERT.SUBTOPICS({topic_id:req.body.topic_id, sub_topic:req.body.subtopic});
		res_data = await db.runSQL(sql);
		if (res_data.err)
			throw new Error('SQL error!');
		let body = {
			err: false,
			message: 'ok',
			subTopic: {
				id: res_data.result.insertId,
				name: req.body.subtopic
			}
		};
		return res.send(body);
	} catch (err) {
		return res.send({err:true, message:err.message});
	}
});
router.post('/api/deleteTopic', auth);
router.post('/api/deleteTopic', async (req, res) => {
	try {
		if (!('topic_id' in req.body))
			throw new Error('Missing \'topic_id\' param!');
		let topic_id = req.body.topic_id;
		if (isNaN(parseInt(topic_id)))
			throw new Error('Topic id must be NUMBER!');
		let getSubTopicCountSQL = db.SQL.SELECT.SUBTOPICCOUNT({topic_id});
		let subTopicCount = await db.runSQL(getSubTopicCountSQL);
		if (subTopicCount.err)
			throw new Error('SQL Error');
		if (subTopicCount.result[0].count > 0)
			throw new Error('To delete Topic you need delete all subtopics first!');
		let deleteTopicSQL = db.SQL.DELETE.TOPIC({topic_id: topic_id});
		let deleteTopicResult = await db.runSQL(deleteTopicSQL);
		if (deleteTopicResult.err)
			throw new Error('SQL Error');
		return res.send({
			err: null,
			message: 'ok'
		});
	} catch (err) {
		return res.send({
			err: true,
			message: err.message
		});
	}
});
router.post('/api/deleteSubTopic', auth);
router.post('/api/deleteSubTopic', async (req, res) => {
	try {
		if (!('subtopic_id' in req.body))
			throw new Error('Missing \'subtopic_id\' param!');
		let subtopic_id = req.body.subtopic_id;
		if (isNaN(parseInt(subtopic_id)))
			throw new Error('SubTopic id must be NUMBER!');
		let countPostsSQL = db.SQL.SELECT.COUNTPOST({subtopic_id: subtopic_id});
		let countPostsData = await db.runSQL(countPostsSQL);
		if (countPostsData.err)
			throw new Error('SQL Error');
		if (countPostsData.result[0].count > 0)
			throw new Error('To delete SubTopic you need delete all posts first!');	
		let deleteSubTopicSQL = db.SQL.DELETE.SUBTOPIC({subtopic_id: subtopic_id});	
		let deletedPostData = await db.runSQL(deleteSubTopicSQL);
		if (deletedPostData.err)
			throw new Error('SQL Error');
		return res.send({
			err: null,
			message: 'ok'
		});
	} catch (err) {
		return res.send({
			err: true,
			message: err.message
		});
	}
});
router.post('/api/deletePost', auth);
router.post('/api/deletePost', async (req, res) => {
	try {
		if (!('post_id' in req.body))
			throw new Error('Missing \'post_id\' param!');
		let post_id = req.body.post_id;
		if (isNaN(parseInt(post_id)))
			throw new Error('Post id must be NUMBER!');
		let getPostFiles = db.SQL.SELECT.FILESBYPOSTID(post_id);
		let postFiles = await db.runSQL(getPostFiles);
		if (postFiles.err)
			throw new Error(postFiles.err);
		for (let i in postFiles.result) {
			let s3 = new AWS.S3({
				params:{
					Key: postFiles.result[i].filename,
					Bucket: 'englishteachblog'
				}
			});
			let result = await s3.deleteObject().send();
			if (result.error)
				throw new Error('Can`t remove file from S3 Bucket!');	
		}	
		let deletePostSQL = db.SQL.DELETE.POST({post_id});	
		let deletedPostData = await db.runSQL(deletePostSQL);
		if (deletedPostData.err)
			throw new Error('SQL Error');	
		return res.send({
			err: null,
			message: 'ok'
		});
	} catch (err) {
		return res.send({
			err: true,
			message: err.message
		});
	}
});

router.get('/api/getPostsByText/:text', async (req, res) => {
	let getPostsSQL = db.SQL.SELECT.POSTBYTEXT(req.params.text);
	let posts = await db.runSQL(getPostsSQL);
	return res.send(posts);
});

router.get('/api/getPostsByTopic/:topic/:subtopic', async (req, res) => {
	let posts = [];
	if (req.params.topic.toString() === 'null') {
		let getPostsSQL = db.SQL.SELECT.POSTBYSUBTOPICS(req.params.subtopic);
		posts = await db.runSQL(getPostsSQL);
	} else 
	if (req.params.subtopic.toString() === 'null') {
		let getPostsSQL = db.SQL.SELECT.POSTBYTOPICS(req.params.topic);
		posts = await db.runSQL(getPostsSQL);
	}
	return res.send(posts);
});

router.get('/api/getPosts', async (req, res) => {
	let getPostsSQL = db.SQL.SELECT.ALLPOSTS;
	let posts = await db.runSQL(getPostsSQL);
	return res.send(posts);
});

router.get('/posts/:postId', async (req, res) => {
	try {
		let postId = req.params.postId;
		let getPostInfoSQL = db.SQL.SELECT.POSTSBYID(postId);
		let post = await db.runSQL(getPostInfoSQL);
		if (post.err)
			throw new Error(err.message);
		if (post.result.length === 0)
			throw new Error('Post not find!');	
		post = post.result[0];
		
		let getPostFilesSQL = db.SQL.SELECT.FILESBYPOSTID(postId);
		let files = await db.runSQL(getPostFilesSQL);
		if (files.err)
			throw new Error(files.err.message);
		if (files.result.length === 0)
			post.files = [];
		else 
			post.files = files.result;	
		return res.render('post', {
			menu:content.ru.menu,
			err:null,
			post:post
		});
	} catch(err) {
		return res.render('post', {
			menu:content.ru.menu,
			err:err.message,
			post:null
		});
	}
});


router.get('/api/posts/:postId', async (req, res) => {
	try {
		let postId = req.params.postId;
		let getPostInfoSQL = db.SQL.SELECT.POSTSBYID(postId);
		let post = await db.runSQL(getPostInfoSQL);
		if (post.err)
			throw new Error(err.message);
		if (post.result.length === 0)
			throw new Error('Post not find!');	
		post = post.result[0];
		let getPostFilesSQL = db.SQL.SELECT.FILESBYPOSTID(postId);
		let files = await db.runSQL(getPostFilesSQL);
		if (files.err)
			throw new Error(files.err.message);
		if (files.result.length === 0)
			post.files = [];
		else 
			post.files = files.result;	
		post.text = post.text.replace(/\n/g, '<br>');	
		return res.send({
			err:null,
			post:post
		});
	} catch(err) {
		return res.send({
			err:err.message,
			post:null
		});
	}
});

module.exports = router;
