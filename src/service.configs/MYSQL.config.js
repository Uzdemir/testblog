const MYSQL = require('mysql');
const MySQLRequestFactory = require('../classes/MYSQL/MySQLRequestFactory').MySQLRequestFactory;
const logger = require('./logger.config');

const MYSQL_logger = logger.getLogger('MYSQL');
let dbConfig = {
	connectionLimit:    process.env.DATABASE_CONNECTIONS    || 30,
	host:               process.env.DATABASE_HOST           || '127.0.0.1',
	user:               process.env.DATABASE_USER           || 'root',
	password:           process.env.DATABASE_PASS           || 'root',
	port:               process.env.DATABASE_PORT           || '3306',
	database:           process.env.DATABASE_T                || 'english_t_blog'
};

MYSQL_logger.info(`DB CONFIGS:${JSON.stringify(dbConfig, null, '\t')}`);

let app = {
	locals:{}
};

app.locals.dbConfig = dbConfig;
app.locals.dbPool = MYSQL.createPool(dbConfig);

let req = {
	app:app
};

const factory = new MySQLRequestFactory(req);
let postsSQL = (filters='') => {
	return `
	SELECT 
		posts.id AS 'id',
		SUBSTRING(posts.text, 1, 100) AS 'text',
		posts.date_create AS 'date_create',
		posts.date_edit AS 'date_edit',
		topics.id AS 'topic_id',
		topics.topic AS 'topic',
		sub_topics.id AS 'sub_topic_id',
		sub_topics.sub_topic AS 'sub_topic',
		posts.subject
	 FROM posts 
		INNER JOIN sub_topics ON posts.sub_topic=sub_topics.id
			INNER JOIN topics ON topics.id=sub_topics.topic${filters};
`;
};
const SQL = {
	SELECT:{
		ALLPOSTS:postsSQL(' ORDER BY posts.date_edit DESC'),
		POSTSBYID:(id) => {
			return `
			SELECT 
				posts.id AS 'id',
				posts.text AS 'text',
				posts.date_create AS 'date_create',
				posts.date_edit AS 'date_edit',
				topics.id AS 'topic_id',
				topics.topic AS 'topic',
				sub_topics.id AS 'sub_topic_id',
				sub_topics.sub_topic AS 'sub_topic',
				posts.subject
			 FROM posts 
				INNER JOIN sub_topics ON posts.sub_topic=sub_topics.id
					INNER JOIN topics ON topics.id=sub_topics.topic WHERE posts.id='${id}';
		`;
		},
		POSTBYDATECREATE:(date_create) => {
			return postsSQL(` WHERE date_create='${date_create}'`);
		},
		POSTBYTEXT:(text) => {
			return postsSQL(` WHERE MATCH (subject,text) AGAINST ('${text}')`);
		},
		POSTBYTOPICS:(topic) => {
			return postsSQL(` WHERE topics.id='${topic}'`);
		},
		POSTBYSUBTOPICS:(subtopic) => {
			return postsSQL(` WHERE sub_topics.id='${subtopic}'`);
		},
		COUNTPOST: ({subtopic_id}) => {
			return `
				SELECT COUNT(*) AS count FROM posts WHERE sub_topic=${subtopic_id};
			`;
		},
		TOPICS:({topic}) => {
			let sql = '';
			if (topic)
				sql = `
					SELECT id, topic FROM topics WHERE topic='${topic}';
				`;
			else
				sql = `
					SELECT id, topic FROM topics;
				`;
			return sql;
		},
		SUBTOPICS:({topic_id, sub_topic}) => {
			let sql = '';
			if (sub_topic)
				sql = `
					SELECT * FROM sub_topics WHERE topic='${topic_id}' AND sub_topic='${sub_topic}';
				`;
			else	
				sql = `
					SELECT id, sub_topic FROM sub_topics WHERE topic='${topic_id}';
				`;	
			return sql;
		},
		SUBTOPICCOUNT: ({topic_id}) => {
			return `
				SELECT COUNT(*) AS count FROM sub_topics WHERE topic='${topic_id}';
			`;
		},
		FILESBYPOSTID:(postId) => {
			return `
				SELECT 
					files.name AS 'filename',
					files.storage AS 'storage',
					files.date_create AS 'date_create',
					files.original_name AS 'original_name'
				 FROM files
					INNER JOIN post_files ON files.id=post_files.file
				 WHERE post_files.post=${postId};	
			`;
		}
	},
	INSERT:{
		FILE:({name, storage, original_name}) => {
			return `
				INSERT INTO files (name, storage, original_name) VALUES ('${name}', '${storage}', '${original_name}');
			`;
		},
		POST:({text, sub_topic, subject}) => {
			return `
				INSERT INTO posts (text, sub_topic, subject) VALUES ('${text}', '${sub_topic}', '${subject}');
			`;
		},
		POST_FILE:({post, file}) => {
			return `
				INSERT INTO post_files (post, file) VALUES ('${post}', '${file}');
			`;
		},
		TOPICS:({topic}) => {
			return `
				INSERT INTO topics (topic) VALUES ('${topic}');
			`;
		},
		SUBTOPICS:({topic_id, sub_topic}) => {
			return `
				INSERT INTO sub_topics (topic, sub_topic) VALUES ('${topic_id}', '${sub_topic}');
			`;
		}
	},
	DELETE: {
		TOPIC:({topic_id}) => {
			return `
				DELETE FROM topics WHERE id=${topic_id};
			`;
		},
		SUBTOPIC: ({subtopic_id}) => {
			return `
				DELETE FROM sub_topics WHERE id=${subtopic_id};
			`;
		},
		POST: ({post_id}) => {
			return `
				DELETE FROM posts WHERE id=${post_id};
			`;
		}
	}
};

const run = (sql, params) => {
	return new Promise((resolve, reject) => {
		factory.mySQL(sql, params||[], (err, inObj, result) => {
			return resolve({err, result});
		});
	});
};

module.exports = {
	factory:factory,
	SQL:SQL,
	runSQL:run
};