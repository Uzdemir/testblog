'use strict';
const logger = require('intel');

logger.basicConfig({
	format:'<%(name)s>[%(date)s]['+ process.pid +'] ==> %(message)s'
});

module.exports = logger;