var Async = require('async');
var MYSQL = require('mysql');
var dLog = function(){};


exports.MySQLTransactionFactory = function(req, dbConfig){
	dLog = req.app.locals.dLog;
	dLog("D LOG IS SOMETHING");
	try{
		if(typeof req.app.locals.dbConfig != 'object' && typeof dbConfig != 'object')
			throw "No DB CONFIG found";
		
		//TODO add choice between cli called dbConfig
		var conn = MYSQL.createConnection(req.app.locals.dbConfig);
		
		this.beginTransaction = function(cb){
			try{
				conn.beginTransaction(
					function(err){
						if(err)
							return cb(err);
						conn.query('SET AUTOCOMMIT = 0', 
							function(err){
								return cb(err);
							}
						);
					}
				);
			}catch(e){
				cb(e);
			}
		}

		this.mySQL = function(sql, val, callback){
			var values = [];
			if(typeof val === 'string' || typeof val === 'number')
				values.push(val);
			else if(val instanceof Array)
				values = val;
			else if(typeof val === 'object')
				values.push(val);
			else if (typeof val === 'function'){
				callback = val;
			}else{
				callback(dLog("illegal call to mySQL with invalid values"));
			}



			for(var i = 0; i < values.length; i++){
				if(values[i] == null)
					continue;
				if(typeof values[i] === 'object'){
					//protect against injection
					dLog("MySQLRequestFactory.js->mySQL(). Found values object " + values[i] + " to remove");
					try{
						//go through all the members of this object and clean them as necessary
						for(var a in values[i]){
							//we'll only substitute numbers and strings
							if(typeof values[i][a] != 'string' && typeof values[i][a] != 'number')
								continue;

							//protect against injection and wrap any string that has non numbers in quotes
							if(typeof values[i][a] === 'string'){
								values[i][a] = values[i][a].replace(/'/g,"''");
								if(values[i][a].match(/\D/))
									values[i][a] = "'" + values[i][a] + "'";
							}	
						}	
					}catch(e){}				
					sql = substituteSqlTokens(sql, values[i]);
					values.splice(i,1);
				}
			}
			
			
			dLog("Submitting SQL = " + sql + " -- values = " + JSON.stringify(values), 'verbose', ['MySQLRequestFactory.js']);
			//protect against injection
			try{
				for(var a in values)
					if(typeof values[a] === 'string')
						values[a] = values[a].replace(/'/g,"''");			
			}catch(e){}
			
			//make the call
			
			conn.query(
				sql, 
				values, 
				function(error, result){
				if( error ){
					var e = {
						location:"MySQLRequestFactory.js->MySQLTransactionFactory->mySQL()->conn.query->anonimousCallback()",
						sql:sql,
						values:values,
						error:error
					}
					var err = dLog(e, 'error', ['MySQLRequestFactory.js']);
					return callback(err);
				}
				return callback(null, result);
			});
		}

		this.commit = function(cb){
			try{
				conn.commit(
					function(err){
						if(err){
							return conn.rollback(function(err){
								conn.end();
								cb(err)
							});
						}
						else{
							conn.end();
							cb(null);
						}
					}
				);
			}catch(e){
				cb(e);
			}
		}

		this.rollback = function(cb){
			conn.rollback(function(err){
				conn.end();
				cb(err)
			});
		}

	}catch(e){
		var err = {
			location:"MySQLRequestFactory.js->MySQLTransactionFactory",
			error:JSON.stringify(e)
		}
		dLog(err,'critical', ['MySQLTransactionFactory']);
	}

}



exports.MySQLRequestFactory = function(req, dbConfig){

	
	if( typeof req === "string" && req === "cli" ){
		if( !dbConfig )
			throw "Database configuration not supplied";

		try{
			if(dbConfig.dLog){
				dLog = dbConfig.dLog;			
			}			
		}catch(e){
			
		}

		dLog('dbConfig arrived as ' + JSON.stringify(dbConfig));

		// we're run from a CLI script, set up a db pool since one doesn't exist.
		var MYSQL = require('mysql');
		req = {
			app: {
				locals: {
					dbPool: MYSQL.createPool(dbConfig)
				}
			}
		}
	} else if(typeof req != "object" || req == null) {
		throw "MySQLRequestFactory requires a request param";
	}

	try{
		if(typeof req.app.locals.dLog === 'function'){
			dLog = req.app.locals.dLog;			
		}
		else if(typeof dbConfig !== 'object' || !dbConfig.dLog)
			throw "dLog not provided in req or incoming dbConfig"
	}catch(e){
		console.log("dLog not supplied in request powering mySQLReqFactory. " + e);
	}
	
	var request = req;
	var factory = this;

	/*
	obj will look like {
		someToken:value,...
		}
		method will go through all items in obj and replace matching tokens in the sql
	*/
	function substituteSqlTokens(sql, tokenHash){

		try{
			for(var a in tokenHash){
				try{
					var token =  a ;
					var r = new RegExp(token, 'g');
					sql = sql.replace(r, tokenHash[a]);
				}catch(e){
					dLog("unable to replace token " + token + " with substitution " + tokenHash[a], 'error');
				}
			}
		}catch(e){
			dLog('sql token substitution failed ' + e,'error');
		}
		return sql;
	}

	this.mySQL = function(sql, val, callback){
		var values = [];
		if(typeof val === 'string' || typeof val === 'number')
			values.push(val);
		else if(val instanceof Array)
			values = val;
		else if(typeof val === 'object')
			values.push(val);
		else if (typeof val === 'function'){
			callback = val;
		}else{
			callback(dLog("illegal call to mySQL with invalid values"));
		}



		for(var i = 0; i < values.length; i++){
			if(values[i] == null)
				continue;
			if(typeof values[i] === 'object'){
				//protect against injection
				dLog("MySQLRequestFactory.js->mySQL(). Found values object " + values[i] + " to remove");
				try{
					//go through all the members of this object and clean them as necessary
					for(var a in values[i]){
						//we'll only substitute numbers and strings
						if(typeof values[i][a] != 'string' && typeof values[i][a] != 'number')
							continue;

						//protect against injection and wrap any string that has non numbers in quotes
						if(typeof values[i][a] === 'string'){
							values[i][a] = values[i][a].replace(/'/g,"''");
							if(values[i][a].match(/\D/))
								values[i][a] = "'" + values[i][a] + "'";
						}	
					}	
				}catch(e){}				
				sql = substituteSqlTokens(sql, values[i]);
				values.splice(i,1);
			}
		}
		
		
		dLog("Submitting SQL = " + sql + " -- values = " + JSON.stringify(values), 'verbose', ['MySQLRequestFactory.js']);
		//protect against injection
		try{
			for(var a in values)
				if(typeof values[a] === 'string')
					values[a] = values[a].replace(/'/g,"''");			
		}catch(e){}
		
		//make the call
		
		request.app.locals.dbPool.query(
			sql, 
			values, 
			function(error, result){
			if( error ){
				var e = {
					location:"MySQLRequestFactory.js->mySQL()->request.app.dbPool.query->anonimousCallback()",
					sql:sql,
					values:values,
					error:error
				}
				var err = dLog(e, 'error', ['MySQLRequestFactory.js']);
				return callback(err,'error');
			}
			return callback(null, request, result);
		});
	}

	var mySQL = this.mySQL; 

	this.end = function(){
		request.app.locals.dbPool.end(); 
	};

	/*
	method tests to see if combination of values in the columns exists as a row in table
	returns the row if it does
	creates new row with these values if not
	*/
	this.mySQLInsertSelect = function(table, columns, values, callback){
		try{
			if(!(columns instanceof Array) || !(values instanceof Array))
				throw ("either columns or values passed in as something other than an array, NO GOOD!");
			if(columns.length != values.length || columns.length == 0)
				throw ("columns length doesn't match values length");
		}catch(e){

			return callback("failed to mySQLInsertSelect " + e);
		}
		var whereString = " where ";
		for(var a = 0; a < columns.length ; a++){
			var comparitor = " = " ;
			//dLog("values[a] arriving as [" + values[a] + "]");
			if(values[a] == null || (typeof values[a] == "string" && values[a].toLowerCase() === "null"))
				comparitor = " is " ;
			whereString += columns[a] + comparitor + wrapInSingleQuotes(values[a]) + " and "
			
		}
		whereString = whereString.replace(/ and $/,"");
		
		mySQL(
			"select id from " + table + whereString,
			null,
			function(err, inObj, res){
				if(err)
					return callback("failed to mySQLInsertSelect [SELECT]" + err);
				if(res.length > 0)
					return callback(null, inObj,res);

				//if we're here, we didn't find it, so let's insert it
				var setString = " set ";
				for(var a = 0; a < columns.length ; a++){
					setString +=  "`" + columns[a] + "` = " + wrapInSingleQuotes(values[a]) + " ,"
				}
				setString = setString.replace(/,$/,"");
				var sql = "INSERT INTO " + table + setString;
				
				mySQL(
					sql,
					null,
					function(err, inObj, res){
						if(err)
							return callback("failed to mySQLInsertSelect [INSERT]" + err);
						var retObj = {id:res.insertId};
						
						
						return callback(null,inObj,[retObj]);
					}
				);
			}
		)
	}


	this.formatSQL = function(){
		try{
			if( arguments.length < 2 ){
				throw "ERROR: TOO FEW ARGUMENTS SUPPLIED";
			}
			var formattedSQL = arguments[0];
			var callback = arguments[arguments.length - 1];


			var lastArgIndex = arguments.length - 2;
			var firstArgIndex = 1;

			var objectArg = arguments[1];
			var objectSupplied = false;
			var callbackSupplied = true;

			if( typeof callback !== "function" ){
				lastArgIndex = arguments.length - 1;
				callback = null;
				callbackSupplied = false;
			}

			// add support for a named replacement
			// for example @$EVENT_ID$@
			// { EVENT_ID: 32 }

			if( (typeof objectArg === "object") && !(objectArg instanceof Array) ){
				// if( /@\$[^$]+\$@/g.exec(formattedSQL) === null ){
				if( formattedSQL.match(/@\$[^$]\$@/g) === null){
					throw("ERROR: key-value store supplied but query does not include any named replacements.")
				}
				objectSupplied = true;
				firstArgIndex = 2;
				for( var key in values ){
					var regex = new RegExp("@$" + key + "$@", "g"); // match @$NAME$@
					formattedSQL = formattedSQL.replace(regex, values[key]);
				}
				if( /@\$[^$]+\$@/g.exec(formattedSQL) === null ){
					throw("ERROR: key-value store supplied but missing some named replacements.")
				}
			}

			var argsSupplied = arguments.length - 1;
			if( callbackSupplied )
				argsSupplied -= 1;
			if( objectSupplied )
				argsSupplied -= 1;

			// var matches = /\$\?\?\?\$/g.exec(formattedSQL);
			var matches = formattedSQL.match(/\$\?\?\?\$/g);
			if( matches !== null && matches.length !== argsSupplied ){
				throw("ERROR: Number of arguments supplied does not match number of replacements in SQL.");
			}

			var args = new Array();
			for(var i = firstArgIndex ; i <= lastArgIndex; i++)
				args.push(arguments[i]);

			var tokenCounter = 0;
			var argIndex = 0;
			while(formattedSQL.match(/\$\?\?\?\$/) && ++tokenCounter){
				try{
					var replaceValue = (args[argIndex])?args[argIndex] : 'NULL';
					formattedSQL = formattedSQL.replace( /\$\?\?\?\$/, replaceValue);
					argIndex++;
				}catch(e){
					throw ("ERROR REPLACING VALUES: " + JSON.stringify(e));
				}
			}
			return {sql:formattedSQL,error:null};
		}catch(e){
			return {sql:null, error:e};
		}
		
	}

	/**
		DEPRECATED METHOD. Supports all old Kahn functionality
		Please use mySQL() instead for all new features. 
	*/
	this.formatAndRunSQL = function(){
		try{
			if( arguments.length < 2 ){
				return "ERROR: TOO FEW ARGUMENTS SUPPLIED";
			}
			var formattedSQL = arguments[0];
			var callback = arguments[arguments.length - 1];


			var lastArgIndex = arguments.length - 2;
			var firstArgIndex = 1;

			var objectArg = arguments[1];
			var objectSupplied = false;
			var callbackSupplied = true;

			if( typeof callback !== "function" ){
				lastArgIndex = arguments.length - 1;
				callback = null;
				callbackSupplied = false;
			}

			

			if( (typeof objectArg === "object") && !(objectArg instanceof Array) ){
				// if( /@\$[^$]+\$@/g.exec(formattedSQL) === null ){
				if( formattedSQL.match(/@\$[^$]\$@/g) === null){
					return callback("ERROR: key-value store supplied but query does not include any named replacements.")
				}
				objectSupplied = true;
				firstArgIndex = 2;
				for( var key in values ){
					var regex = new RegExp("@$" + key + "$@", "g"); // match @$NAME$@
					formattedSQL = formattedSQL.replace(regex, values[key]);
				}
				if( /@\$[^$]+\$@/g.exec(formattedSQL) === null ){
					return callback("ERROR: key-value store supplied but missing some named replacements.")
				}
			}

			var argsSupplied = arguments.length - 1;
			if( callbackSupplied )
				argsSupplied -= 1;
			if( objectSupplied )
				argsSupplied -= 1;

			// var matches = /\$\?\?\?\$/g.exec(formattedSQL);
			var matches = formattedSQL.match(/\$\?\?\?\$/g);
			if( matches !== null && matches.length !== argsSupplied ){
				return callback("ERROR: Number of arguments supplied does not match number of replacements in SQL.");
			}

			var args = new Array();
			for(var i = firstArgIndex ; i <= lastArgIndex; i++)
				args.push(arguments[i]);

			var tokenCounter = 0;
			var argIndex = 0;
			while(formattedSQL.match(/\$\?\?\?\$/) && ++tokenCounter){
				try{
					var replaceValue = (args[argIndex])?args[argIndex] : 'NULL';
					formattedSQL = formattedSQL.replace( /\$\?\?\?\$/, replaceValue);
					argIndex++;
				}catch(e){
					return callback("ERROR REPLACING VALUES: " + JSON.stringify(e));
				}
			}


			request.app.locals.dbPool.query(
				formattedSQL,
				function(error, result){
					try{
						if( error ){
							try{
								var e = {
									location:"MySQLRequestFactory.js->formatAndRunSQL()->request.app.dbPool.query->annonimousCallback()",
									sql:formattedSQL,
									error:error
								}
								dLog(e, 'error', ['MySQLRequestFactory.js']);
							}catch(e){}
							error.submittedSQL = formattedSQL;
							return callback(error);
						}
						var e = {
									location:"MySQLRequestFactory.js->formatAndRunSQL()->request.app.dbPool.query->annonimousCallback()",
							sql:formattedSQL,
							result:result
						}
						dLog(e, 'debug', ['MySQLRequestFactory.js']);
						result.submittedSQL = formattedSQL;
						return callback(null, result);
					}catch(e){
						return callback("failed after query: " + e);
					}
				}
			);
		}catch(e){
			if(callback)
				return callback("formatAndRunSQL() failed: " + e);
			return "formatAndRunSQL() failed - No callback provided and : " + e;
		}
	}
	
	/**
	 * Useage obj should look like
	 * {
	 * 	  valueField:"name",		
	 *    i18Parent:incomingArtistJSON,
	 *    table:"acts",
	 *    columnName:"act_name_label_id",
	 * }
	 */
	this.i18insertUpdate = function(obj, cb){		
		dLog("i18insertUpdate()");
		try{
			var ar = [];					
			//add an entry for the name of the artist in each language
			for(var i = 0; i < obj.i18Parent.i18n_values.length; i++){
				var item = {};
				item["value"] = obj.i18Parent.i18n_values[i][obj.valueField];
				item["table"] = obj.table;
				item["table_row_id"] = obj.i18Parent.DBID;
				item["column_name"] = obj.columnName;
				item["language"] = obj.i18Parent.i18n_values[i].language;
				ar.push(item)
			}	
			//THIS HAS TO BE AN EACH SERIES or stuff goes VERY wrong
			Async.eachSeries(ar, 
					i18nInsertLocal,
				function(err){
					if(err){						
						return cb(dLog("failed to do i18 " + err, 'error'));	
					}
					return cb(null);
				}
			);				
		}catch(e){			
			return cb(dLog("failed to set i18n values " + e,'error'));
		}		
	}

	this.i18nInsert = function(item, outerCallback){
		//table, table_row_id, label_id_column_name, string, language, callback
		
		Async.series(
			[
			function(seriesCallback){
				try{
					//test if language exists and if not add it
					factory.mySQLInsertSelect("languages", ["language"], [item.language],
						function(err, inObj, res){
							if(err)
								return seriesCallback("i18Insert failed " + err);	
							item.languageDBID = res[0].id
							return seriesCallback(null);						
						}
					 );
					
				}catch(e){ 
					return seriesCallback("i18nInsert failed " + e);
				}
			},
			function(seriesCallback){
				//test if label id is set in this row and if not introduce it
				dLog("looking for existing label id");
				try{						
					mySQL("SELECT " + item.column_name + " from " + item.table + " where id = " + item.table_row_id,
						null,
						function(err, inObj, res){
												
							if(err)
								return seriesCallback("failed in sql call " + err);
							if(res[0][item.column_name] == null){
								//no label, so add one
								dLog("VALUE IS NULL");
								mySQL(
									"INSERT INTO labels (id) VALUES (null)",
									null,
									function(err,inObj,resp){
										if(err)
											return seriesCallback("can't add label" + err);
										item.labelDBID = resp.insertId;
										dLog("inserted label, new id = " + resp.insertId);
										mySQL(
											"UPDATE " + item.table + " SET " + item.column_name + " = ? WHERE id = ?" ,
											 [resp.insertId, item.table_row_id],
											function(err, inObj, resp){
												if(err)
													return outerCallback("can't add label to " + item.table + "." + item.column_name + ": "+ e);
												dLog("added label to " + item.table + "." + item.column_name);
												return seriesCallback(null)
											}
										);
									}			
								);
							}else{//label exists, grab it and move on
								item.labelDBID = res[0][item.column_name];
								return seriesCallback(null);
							}							
						}
					);
				}catch(e){
					return seriesCallback("unable to run language query " + e);
				}
			},
			function(seriesCallback){
				//test if string_label is there and if not introduce it
				factory.mySQLInsertSelect
				try{	
					//add a row to label_strings if it doesn't exist
					dLog("Adding row to label_strings");
					factory.mySQLInsertSelect(
						"label_strings",
						["languages_id", "labels_id"],
						[item.languageDBID, item.labelDBID],
						function(err, inObj, resp){
							try{
								if(err)
									throw ("unable to find/add label_string " + err);
								var stringLabelId = resp[0].id;
								mySQL(
									"UPDATE label_strings set text = ? where id = ?",
									[item.value, stringLabelId],
									function(err, inObj, resp){
										try{
											if(err)
												throw ("can't update value in string label " + err);
											dLog("added value " + item.value);
											seriesCallback(null);
										}catch(e){
											return seriesCallback(e);
										}
									}
								);
							}catch(e){
								return seriesCallback(e);
							}
						}
					);					
					
				}catch(e){
					return seriesCallback("unable to run language query " + e);
				}
			}
			],
			function (err){
				if(err)
					return outerCallback(err);
				return outerCallback(null)
			}
		);//end series
	}
	
	var i18nInsertLocal = this.i18nInsert;

	function wrapInSingleQuotes(val){
		if(typeof val == "string" && val != "NULL" && !val.match(/^FROM_UNIXTIME\(\s*\d+\s*\)$/))
			return "'" + val + "'";
		return val;
	}
}