
$(document).ready(function(){
	var postData = $('#postData tbody'),
		nextPage = $('#nextPage'),
		prevPage = $('#prevPage'),
        main_menu = $('#main_menu'),
        filterOption = $('#filterOption'),
        currentI = 0;

	var pointer = 0;
	var skip = 7;
	var postDataBuffer = posts.result;
	displayBufferData();


	nextPage.click(function(){
		if ((pointer+skip) >= postDataBuffer.length){
			return;
		} else{
			pointer+=skip;
		}
		displayBufferData();
	});

	prevPage.click(function(){
		if (pointer <= 0){
			pointer = 0;
		} else{
			pointer-=skip;
		}
		displayBufferData();
	});

    var timer_id = null;
	filterOption.on('keyup', function(e){
		clearTimeout(timer_id);
		setTimeout(function () {
			filterEvents();
        }, 300);
		if (e.keyCode === 27){
			filterOption.val('');
			postDataBuffer = events;
			displayBufferData();
		}
	});

	function displayBufferData(){
        postData.html('');
		for(var i = pointer; i < (skip + pointer) && i < postDataBuffer.length; i++){
			postData.append(templates.post(postDataBuffer[i], i));
		}
    }
    
    postData.on('click', '.deletePost', function() {
        var post_id = $(this).attr('post');
        currentI = $(this).parents('tr').attr('id');
        console.log(currentI);
        var r = confirm('Do you want to delete post '+ post_id +'');
        if (r === true) {
            $.post('/api/deletePost', {post_id: post_id}, function(result){
                if (result.err) 
                    return alert(result.message);
                postDataBuffer.splice(currentI, 1);
                displayBufferData();    
            });
        }
    });

	function filterEvents(){
		postDataBuffer = [];
		var flag = filterOption.val().toString().toLowerCase();
		for(var i = 0; i < events.length; i++){
			if (events[i].unique_event_code == null)
                events[i].unique_event_code = 'null';
			if (events[i].name.toString().toLowerCase().indexOf(flag) >= 0 ||
				events[i].unique_event_code.toString().toLowerCase().indexOf(flag) >= 0  ||
				templates.formatDate(events[i].start_date).indexOf(flag) >= 0  ||
				templates.formatDate(events[i].end_date).indexOf(flag) >= 0) {
					postDataBuffer.push(events[i]);
			}
		}
		displayBufferData();
	}


});



var templates = {
	formatDate:function(date){
		var formatDate = new Date(date);
		var month = formatDate.getMonth();
		var date = formatDate.getDate()+1;
		if (month < 10)
			month = '0'+month
		if (date < 10)
			date = '0'+date
		var newDate = formatDate.getFullYear()+'-'+month+'-'+date;
		return newDate;
	},
	post:function(post, i){
		return '<tr id="'+ i +'">'+
				'<td>'+
					'<button eventCode="'+post.id+'" class="btn btn-default editEvent">'+
						'<img class="postIcon" src="/icons/edit.png"/>'+
					'</button>'+
				'</td>'+
				'<td code="'+ post.id +'">'+ post.id +'</td>'+
				'<td code="'+ post.topic_id +'">'+ post.topic +'</td>'+
				'<td code="'+ post.sub_topic_id +'">'+ post.sub_topic +'</td>'+
				'<td>'+post.subject+'</td>'+
				'<td style="overflow: hidden; width: 250px;">'+post.text+'</td>'+
				'<td>'+templates.formatDate(post.date_create)+'</td>'+
                '<td>'+templates.formatDate(post.date_edit)+'</td>'+
                '<td>'+
                '<button post="'+post.id+'" class="btn btn-default deletePost">'+
                    '<img class="postIcon" src="/icons/delete.png"/>'+
                '</button>'+
            '</td>'+
			'</tr>'	
	}
}