$(document).ready(function(){
	var addPost = $('#addPost');
	var addTopic = $('#addTopic');
	var managePosts = $('#managePosts');
	var addPostBTN = $('#addPostBTN');
	var addTopicBTN = $('#addTopicBTN');
	var managePostsBTN = $('#managePostsBTN');
	var topic_d = $('#topic');
	var subtopic_d = $('#subtopic');
	var topicsList = $('#topicsList');
	var addSubTopic = $('#addSubTopic');
	var newSubTopic = $('#newSubTopic');
	var subTopicsList = $('#subTopicsList');
	var newTopic = $('#newTopic');
	var addNewTopicBTN = $('#addNewTopicBTN');
	var addNewSubTopicBTN = $('#addNewSubTopicBTN');
	var newFileBin = $('#newFileBin');
	var addNewFileBTN = $('#addNewFileBTN');
	var saveNewPost = $('#saveNewPost');
	var postText = $('#postText');
	var subject = $('.subject');
	var inFiles = $('.inFiles');
	var loader1 = $('#l1');
	var loader2 = $('#l2');
	var currentTopic = null;

	function loaderControl(flag, loader){
		if (flag) {
			loader.animate({
				opacity:100
			}, 500);
		} else {
			loader.animate({
				opacity:0
			}, 500);
		}
	}

	function updateTopics() {
		topic_d.html('');
		topicsList.html('');
		for (var i in topics){
			if (i !== 'length') {
				topic_d.append('<option value='+ topics[i].id +'>'+ topics[i].topic +'</option>');
				topicsList.append('<a class="list-group-item" href="#" code="'+ 
					topics[i].id +'">'+
					'<img src="icons/delete.png" topic="'+ topics[i].id +'" class="delete deleteTopic"/>'+ 
					topics[i].topic +' <span class="badge">SubTopics '+ 
					topics[i].subtopics.length +
					'</span></a>');
			}
		}
	}
	updateTopics();

	topicsList.on('click', '.deleteTopic', function(){
		$.post('/api/deleteTopic', {
			topic_id: $(this).attr('topic')
		}, function(res) {
			if (res.err)
				return alert(res.message);
			alert(res.message);	
			window.location.reload();
		});
	});

	subTopicsList.on('click', '.deleteSubTopic', function(){
		$.post('/api/deleteSubTopic', {
			subtopic_id: $(this).attr('subTopic')
		}, function(res) {
			if (res.err)
				return alert(res.message);
			alert(res.message);	
			window.location.reload();
		});
	});

	topic_d.on('change', function(){
		var topic_id = $(this).val();
		subtopic_d.html('<option value="null">SubTOPIC</option>');
		for (var i in topics[topic_id].subtopics){
			if (i !== 'length')
				subtopic_d.append('<option value='+ topics[topic_id].subtopics[i].id +'>'+ topics[topic_id].subtopics[i].sub_topic +'</option>');
		}
	});

	topicsList.on('click', 'span', function(){
		var topic_id = $(this).parent().attr('code');
		currentTopic = topic_id;
		newSubTopic.val('');
		subTopicsList.html('');
		for (var sub_t in topics[topic_id].subtopics)
			if (topics[topic_id].subtopics.length != 0)
				if (sub_t !== 'length')
					subTopicsList.append('<a class="list-group-item" href="#" code="'+ 
						topics[topic_id].subtopics[sub_t].id +'">'+ 
						topics[topic_id].subtopics[sub_t].sub_topic +
						'<img src="icons/delete.png" subTopic="'+ topics[topic_id].subtopics[sub_t].id +'" class="delete deleteSubTopic"/>'+
						'</a>');
		addSubTopic.modal('show');
	});

	addNewTopicBTN.click(function(){
		var topic_val = newTopic.val();
		loaderControl(true, loader1);
		$.post('/api/addTopic', {
			topic:topic_val
		}, function(res){
			loaderControl(false, loader1);
			if (res.err)
				return alert('Error adding new topic!');
			topicsList.append('' +
				'<a class="list-group-item" href="#" code="'+ res.topic.id +'">'+ res.topic.name +' <span class="badge">SubTopics 0</span></a>' +
			'');
			newTopic.val('');
			topics[res.topic.id] = {
				id: res.topic.id,
				topic: res.topic.name,
				subtopics: {
					length: 0
				}
			};
			updateTopics();
			return alert('Success!');
		});
	});

	addNewSubTopicBTN.click(function(){
		var sub_topic_val = newSubTopic.val();
		loaderControl(true, loader2);
		$.post('/api/addSubTopic', {
			topic_id:currentTopic,
			subtopic:sub_topic_val
		}, function(res){
			loaderControl(false, loader2);
			if (res.err)
				return alert('Error adding new topic! ' + res.message);
			subTopicsList.append('<a class="list-group-item" href="#" code="'+ res.subTopic.id +'">'+ res.subTopic.name +'</a>');
			var sub_t_len = topicsList.find('[code='+ currentTopic +']').find('span').html().replace('SubTopics ', '');
			sub_t_len = parseInt(sub_t_len);
			sub_t_len++;
			topicsList.find('[code='+ currentTopic +']').find('span').html('SubTopics '+sub_t_len);
			topics[currentTopic].subtopics[res.subTopic.id] = {
				id: res.subTopic.id,
				sub_topic: res.subTopic.name
			};
			newSubTopic.val('');
			return alert('Success!');
		});
	});

	addPostBTN.click(function(){
		addTopic.hide();
		addPost.show();
		managePosts.hide();
	});

	addTopicBTN.click(function(){
		addTopic.show();
		addPost.hide();
		managePosts.hide();
	});

	managePostsBTN.click(function(){
		addTopic.hide();
		addPost.hide();
		managePosts.show();
	});

	addNewFileBTN.click(function(){
		inputFile = null;
		newFileBin.click();
	});

	var inputFiles = null;
	var validForm = null;

	newFileBin.on('change', function(){
		inFiles.html('');
		validForm = new FormData();
		for (var i = 0; i < this.files.length; i++) {
			validForm.append('file'+i, this.files[i]);
			inFiles.append('<span class="badge">'+ this.files[i].name +'</span>');
		}
	});

	saveNewPost.click(function(){
		if (validForm == null)
			validForm = new FormData();
		loaderControl(true, loader1);	
		validForm.append('topic', topic_d.val());
		validForm.append('sub_topic', subtopic_d.val());
		validForm.append('description', postText.val());
		validForm.append('subject', subject.val());
		$.ajax({
			url:'/api/addPost',
			data:validForm,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function(data){
				validForm = null;
				loaderControl(false, loader1);
				if (data.err)
					return alert('Error adding post!');
				topic_d.val('null');
				subtopic_d.val('null');
				postText.val('');
				subject.val('');
				inFiles.html('');
				alert('Post was added!');
				window.location.reload();
			}
		});
	});



});