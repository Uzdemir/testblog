$(document).ready(function(){
	var topicsDOM = $('.topics');
	var loader = $('.loader');
	var posts = $('.posts');
	var postMoreInfo = $('#postMoreInfo');
	var searchFlag = false;
	var searchIcon = $('.searchIcon');
	var searcher = $('.searcher');
	var activeteFilter = $('.activeteFilter');

	$('.list-group').on('ondblclick', function(){
		return;
	});
	for (var i in topics) {
		topicsDOM.append(''+
			'<a class="list-group-item topicItem" id="'+ topics[i].id +'">'+ topics[i].topic +'<span class="badge">subtopics</span></a>'+
		'');
		topicsDOM.append(''+
			'<div class="row">'+
				'<div class="col-lg-2"></div>'+
				'<div class="col-lg-10">'+
					'<ul class="list-group subtopics" style="display:none;" display="false" topic="'+ topics[i].id +'"></ul>'+
				'</div>'+
			'</div>'+		
		'');
		var subtopicsForTopic = topicsDOM.find('[topic='+ topics[i].id +']');

		for (var j in topics[i].subtopics) {
			subtopicsForTopic.append(''+
				'<a class="list-group-item subTopicItem" id="'+ topics[i].subtopics[j].id +'">'+ topics[i].subtopics[j].sub_topic +'</a>'+
			'');
		}
	}
	topicsDOM.on('click', 'span', function(){
		var topic_id = $(this).parent().attr('id');
		var displayItem = topicsDOM.find('[display="true"]');
		if (displayItem.attr('topic') != topic_id) {
			displayItem.attr('display', false);
			displayItem.hide();
		}
		var subtopicsForTopic = topicsDOM.find('[topic='+ topic_id +']');
		if (subtopicsForTopic.attr('display') == 'true') {
			subtopicsForTopic.hide();
			subtopicsForTopic.attr('display', false);
		} else {
			subtopicsForTopic.show();
			subtopicsForTopic.attr('display', true);
		}
	});
	topicsDOM.on('click', 'a', function(e){
		if ($(e).prop('target').tagName === 'SPAN')
			return;
		var active = topicsDOM.find('.active');
		active.removeClass('active');
		if (active.attr('id') != $(this).attr('id') || active.attr('topic') != $(this).attr('topic'))
			$(this).addClass('active');	
	});

	activeteFilter.click(function(){
		var active = topicsDOM.find('.active');
		var topic = null;
		var subtopic = null;
		if (active.is('.subTopicItem')) {
			subtopic = active.attr('id');
		} else {
			topic = active.attr('id');
		}
		loaderControl(true);
		var url = '/api/getPostsByTopic/'+topic+'/'+subtopic;
		console.log(url);
		$.get(url, function(data){
			loaderControl(false);
			posts.html('');
			if (!data.err) {
				for (var i = 0; i < data.result.length; i++){
					posts.append(templates.post(data.result[i]));
				}
			}
		});

	});

	function loaderControl(flag){
		if (flag) {
			loader.animate({
				opacity:100
			}, 500);
		} else {
			loader.animate({
				opacity:0
			}, 500);
		}
	}

	function dateFormater(date){
		var date = new Date(date);
		var formatedDate = {
			yyyy:date.getFullYear()
		};
		formatedDate.mm = date.getMonth();
		formatedDate.dd = date.getDate();
		return `${formatedDate.yyyy}-${formatedDate.mm}-${formatedDate.dd}`;
	}

	loaderControl(true);
	$.get('/api/getPosts', function(data){
		if (!data.err) {
			for (var i = 0; i < data.result.length; i++){
				posts.append(templates.post(data.result[i]));
			}
		}
		loaderControl(false);
	});

	posts.on('click', '.post', function(){
		var post = $(this);
		loaderControl(true);
		$.get('/api/posts/'+post.attr('id'), function(data){
			loaderControl(false);
			if (data.err)
				return alert(data.err);
			postMoreInfo.find('.modal-title').html(data.post.subject);
			postMoreInfo.find('.topicModal').html(data.post.topic);
			postMoreInfo.find('.sub_topicModal').html(data.post.sub_topic);
			postMoreInfo.find('.text').html(data.post.text);
			postMoreInfo.find('.inNewTab').attr('href', window.location.origin+'/posts/'+post.attr('id'));
			var files = postMoreInfo.find('.filesModal');
			files.html('');
			for (var i = 0; i < data.post.files.length; i++){
				files.append(''+
					'<img class="postFileIcon" src="/icons/file.png"/>'+
					'<a href="'+ data.post.files[i].storage+'/'+ data.post.files[i].filename +'" target="_blank">'+ data.post.files[i].original_name +'</a>'+
				'');
			}
			postMoreInfo.modal('show');
		});
	});

	searchIcon.click(function(){
		if (searchFlag){
			searcher.animate({
				width:'0%'
			}, 500);
			searcher.hide();
			searchFlag = false;
		} else {
			searcher.show();
			searcher.animate({
				width:'100%'
			}, 500);
			searchFlag = true;
		}
	});

	var templates = {
		post:function(post){
			return ''+
				'<div class="post row col-lg-6"  id="'+ post.id +'">'+
					'<div class="col-lg-10">'+
						'<div class="postTitle">'+ post.subject +'</div>'+
						'<blockquote class="blockquote postPrew" style="overflow: hidden;">'+ post.text +'</blockquote>'+
					'</div>'+
				  '</div>'+
			'';
		},
		morePosts:''+
			'<div class="post row">'+
				'<div class="col-lg-5"></div>'+
				'<div class="col-lg-1"><img class="img-rounded morePostsIcon" src="/icons/morePosts.png"/></div>'+
				'<div class="col-lg-6"></div>'+
			  '</div>'+
		''
	};
});