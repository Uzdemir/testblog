$(document).ready(function(){
    $('#logBTN').click(function(){
        $.post('/login', {login: $('#login').val(), password: $('#password').val()}, function(res) {
            if (res.err)
                return alert(res.err);
            alert('Success, redirecting to admin page ...');
            setCookie('token', res.data.token, {expires: 3600});
            window.location = window.location.origin+'/admin';        
        });
    });
    
});

function setCookie(name, value, options) {
    options = options || {};
    var expires = options.expires;
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}