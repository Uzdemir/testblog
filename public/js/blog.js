$(document).ready(function(){
	var posts = $('.posts');
	var searchIcon = $('.searchIcon');
	var searcher = $('.searcher');
	var morePosts = $('.morePostsIcon');
	var loader = $('.loader');
	var postMoreInfo = $('#postMoreInfo');
	var searchFlag = false;

	function loaderControl(flag){
		if (flag) {
			loader.animate({
				opacity:100
			}, 500);
		} else {
			loader.animate({
				opacity:0
			}, 500);
		}
	}
	loaderControl(true);
	$.get('/api/getPosts', function(data){
		if (!data.err) {
			for (var i = 0; i < data.result.length; i++){
				posts.append(templates.post(data.result[i]));
			}
			posts.append(templates.morePosts);
		}
		loaderControl(false);
	});

	posts.on('click', '.post', function(){
		var post = $(this);
		loaderControl(true);
		$.get('/api/posts/'+post.attr('id'), function(data){
			loaderControl(false);
			if (data.err)
				return alert(data.err);
			postMoreInfo.find('.modal-title').html(data.post.subject);
			postMoreInfo.find('.topicModal').html(data.post.topic);
			postMoreInfo.find('.sub_topicModal').html(data.post.sub_topic);
			postMoreInfo.find('.text').html(data.post.text);
			postMoreInfo.find('.inNewTab').attr('href', window.location.origin+'/posts/'+post.attr('id'));
			var files = postMoreInfo.find('.filesModal');
			files.html('');
			for (var i = 0; i < data.post.files.length; i++){
				files.append(''+
					'<img class="postFileIcon" src="/icons/file.png"/>'+
					'<a href="'+ data.post.files[i].storage+'/'+ data.post.files[i].filename +'" target="_blank">'+ data.post.files[i].original_name +'</a>'+
				'');
			}
			postMoreInfo.modal('show');
		});
	});

	searchIcon.click(function(){
		if (searchFlag){
			searcher.animate({
				width:'0%'
			}, 500);
			searcher.hide();
			searchFlag = false;
		} else {
			searcher.show();
			searcher.animate({
				width:'100%'
			}, 500);
			searchFlag = true;
		}
	});
});
function dateFormater(date){
	var date = new Date(date);
	var formatedDate = {
		yyyy:date.getFullYear()
	};
	formatedDate.mm = date.getMonth();
	formatedDate.dd = date.getDate();
	return `${formatedDate.yyyy}-${formatedDate.mm}-${formatedDate.dd}`;
}

var templates = {
	post:function(post){
		return ''+
			'<div class="post row" id="'+ post.id +'">'+
				'<div class="col-lg-1">'+
					'<img class="img-rounded postIcon" src="/icons/postIcon.png"/>'+
				'</div>'+
				'<div class="col-lg-10">'+
					'<div class="postTitle">'+ post.subject +'</div>'+
					'<blockquote class="blockquote postPrew">'+ post.text +'</blockquote>'+  
		 			'<h5 class="postDate" style="float:right">Created: '+ dateFormater(post.date_create) +'</h5>'+
		 			'<h5 class="postDate" style="float:right">Updated: '+ dateFormater(post.date_edit) +'</h5>'+
				'</div>'+
	  		'</div>'+
		'';
	},
	morePosts:''+
		'<div class="post row">'+
			'<div class="col-lg-5"></div>'+
			'<div class="col-lg-1"><img class="img-rounded morePostsIcon" src="/icons/morePosts.png"/></div>'+
			'<div class="col-lg-6"></div>'+
  		'</div>'+
	''
};