var flag = true;

$(document).ready(function () {
	
	var mainInfo = $('.mainInfo');
	var mainMenuItem = $('#mainMenuItem');
	var contentMaintext = $('.contentMaintext');

	$('.content').click(function (e) {
		e.preventDefault();
		if (flag){
			mainInfo.css({
				'margin-top':'1000px',
				'opacity': '0'
			});
			$('.contentMaintext').animate({
				'margin-top': '-500px',
				'opacity': '0'
			}, 100, function () {
				contentMaintext.hide();
				mainInfo.show();
				mainInfo.animate({
					'margin-top':'5%',
					'opacity': '1'
				}, 500);
				flag = false;
			});
		} else{
			contentMaintext.css({
				'margin-top':'1000px',
				'opacity': '0'
			});
			$('.mainInfo').animate({
				'margin-top': '-500px',
				'opacity': '0'
			}, 100, function () {
				mainInfo.hide();
				contentMaintext.show();
				contentMaintext.animate({
					'margin-top':'5%',
					'opacity': '1'
				}, 500);
				flag = true;
			});
		}
	});

});